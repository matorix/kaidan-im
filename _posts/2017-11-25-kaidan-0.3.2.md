---
layout: post
title: "Kaidan 0.3.2 released"
date: 2017-11-25 21:31:00 +01:00
author: lnj
---

## Changelog
 * Added AppImage build script (#138) (JBB)
 * Use relative paths to find resource files (#143) (LNJ)
 * Source directory is only used for resource files in debug builds (#146) (LNJ)

**Download**: [kaidan-v0.3.2.tar.gz](https://invent.kde.org/KDE/kaidan/-/archive/v0.3.2/kaidan-v0.3.2.tar.gz), [kaidan-x86_64.AppImage](https://archive.kaidan.im/kaidan/0.3.2/kaidan-x86_64.AppImage)
